var express = require('express');
var router = express.Router();

//require controller
const operatorController = require('../controller/operator');

router.get('/',operatorController.getOperatorPage);

router.post('/', operatorController.createOperator);
router.get('/operator-edit',operatorController.getEditOperator);
router.post('/update-operator',operatorController.updateOperator);
router.post('/delete-operator', operatorController.deleteOperator);
router.post('/sendMsgOperator', operatorController.sendMsgOperator);
router.post('/checkActivity', operatorController.checkActiveOperator);
router.post('/testing-mail',operatorController.testMailSend);

module.exports = router;