var express = require('express');
var router = express.Router();

const dashboardController = require('../controller/dashboard');
router.get('/',dashboardController.getdashboard);
router.post('/map-customer',dashboardController.mapCustomer);
router.get('/get-customer/:id',dashboardController.getCustomer);

module.exports = router;
