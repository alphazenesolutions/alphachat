const express = require('express');
const router = express.Router();

//require controller
const userController = require('../controller/users');

router.get('/',userController.getLoginPage);
router.post('/',userController.loginUser);

router.get('/forgot-password',userController.getForgotPassword);
router.post('/forgot-password',userController.postForgotPassword);

router.get('/logout',userController.logout);
module.exports = router;