var express = require('express');
var router = express.Router();
var setting = require('../config/settings.json');
var path = require('path');
var fs = require('fs');

router.get('/',async(req,res)=>{
    var content;
    fs.readFile(path.join(__dirname, "../public","/js/pixeltalk_script.js"), 'utf8', function (err, data) {
        if (err) {
            process.exit(1);
        }
        content = data.replace('BASE_URL',setting.Basic_URL);
res.send(content)
    });
});

module.exports = router;