var express = require('express');
var router = express.Router();


// //require controller
const projectController = require('../controller/projects');
const CustomerController = require('../controller/project_customer');
const appearanceController = require('../controller/project_appearance');
const scriptController = require('../controller/project_script');
const projectSettingController = require('../controller/project_setting');
const projectChatController = require('../controller/project_chat');
const notificationController = require('../controller/notification');

router.get('/', projectController.getProjects);

router.get('/create-project', projectController.getCreateProject);
router.post('/create-project', projectController.postCreateProject);

router.get('/project-edit/:id', projectController.editProject);
router.post('/project-update', projectController.updateProject);
router.post('/updateFav', projectController.updateFav);

router.get('/project-customers/:id', CustomerController.getProjectCustomer);
router.post('/delete-customer', CustomerController.deleteCustomer);

router.get('/project-appearance/:id', appearanceController.getProjectAppearance);
router.post('/widget-setting/:id', appearanceController.widgetSetting);

router.get('/project-script/:id', scriptController.getProjectScript);

router.get('/project-settings/:id', projectSettingController.getProjectSetting);
router.post('/delete-project',projectSettingController.deleteProject);
router.post('/project-status', projectSettingController.statusControl);
router.post('/expires-project', projectSettingController.expiresProject);

router.get('/chat/:id', projectChatController.getUser);
router.post('/get-message', projectChatController.getMessage);

router.get('/notification/:id', notificationController.getCustomer);
router.get('/notification-get', notificationController.getNotification);

module.exports = router;
