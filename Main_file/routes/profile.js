var express = require('express');
var router = express.Router();

//require controller
const profileController = require('../controller/profile');

router.get('/',profileController.getProfilePage);
router.post('/update-profile',profileController.updateProfile);

module.exports = router;