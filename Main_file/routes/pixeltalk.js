var express = require('express');
var router = express.Router();

//require controller 
const pixController = require('../controller/pixeltalk');

router.post('/checkProject',pixController.checkProject);
router.post('/pxCustomer/:id',pixController.serverRequest);
router.post('/send-message',pixController.sendMessage);
router.post('/update-customer',pixController.updateCustomer);
router.post('/find-customer',pixController.pxcustomer);

module.exports = router;