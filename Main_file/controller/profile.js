const commonModel = require('../models/common');
const md5 = require('md5');
const moment = require('moment');
var dateTime = new Date()
exports.getProfilePage=async(req, res)=>{
    if(req.session.Id){
        var where = {
            id:req.session.Id
        }
        await commonModel.selectData('id,name,email,project_id','pt_users',where).then(async findNotification=>{
            var userDetails
            var fn = findNotification[0].name.split(' ');
            userDetails = findNotification[0]
            userDetails.first_name= fn[0]
            userDetails.last_name= fn[1]

            res.render('profile',{ userDetails:userDetails,users:req.session.user,nm:req.session.name,access_level:req.session.al});
        }).catch(err=>{
            res.json({status:false, message:'User not found.'});
        });
    }else{
        res.redirect('/')
    }
}

exports.updateProfile=async(req,res)=>{
    var uData = {
        name:req.body.fname+' '+req.body.lname,
        password:md5(req.body.password),
        updatedAt:moment(dateTime).format("YYYY-MM-DD HH:mm:ss")
    } 
    var where = {id:req.body.id}
    req.session.user = req.body.fname+' '+req.body.lname
    req.session.name= req.body.fname[0]+req.body.lname[0]
    await commonModel.updateData('pt_users',uData,where).then(async updtProfile=>{
        res.json({status:true,message:'Profile updated.'})
    }).catch(err=>{
        res.json({status:false, message:'Profile not updated.'});
    });
}