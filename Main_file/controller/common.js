const moment = require('moment');

const commonModel = require('../models/common'); 
const notificationModel = require('../models/notification');

async function getCustomer(project_id){
    var c = '';
    var where
    if(project_id!=''){
        where = {
            project_id : project_id
        }
    }else{
        where = '';
    }
    await notificationModel.findData('msg_read,project_id,customer_id,updatedAt','pt_notifications',where).then(async notification=>{
        var projectNameArray = [];
        for await (let n of notification) { 
            projectNameArray.push({
                msg_read:n.msg_read,
                updatedAt:moment(n.updatedAt).fromNow(),
                pn:await getProjectName(n.project_id),
                cn:await getCustomerName(n.customer_id)
            });
        }
        c = JSON.stringify(projectNameArray)
    }).catch(err=>{
        console.log(err)
    });
    return c
}

async function getProjectName(id){
    var p 
    var where = {
        id:id
    }
    await commonModel.selectData('id,project_name','pt_projects',where).then(pn=>{
        p =pn[0]
    }).catch(err=>{
        console.log(err)
    });
    return p
}

async function getCustomerName(id){
    var c 
    var where = {
        id:id
    }
    await commonModel.selectData('id,name,email','pt_customers',where).then(cn=>{
        c =cn[0]
    }).catch(err=>{
        console.log(err)
    });
    return c
}

module.exports= {getCustomer, getProjectName};