const crypto = require('crypto');

//require common model
const commonModel = require('../models/common');
const settings = require('../config/settings.json');

exports.getProjectScript=async(req, res)=>{
    if(req.session.Id){
        var wh = {id:req.params.id}
        //get project-script page.
        await commonModel.selectData('*','pt_projects',wh).then(async getProject=>{
            var encId = encrypt(req.params.id)
            var url = `<div id="pixeltalk" ref="${encId}"><script src="${settings.Basic_URL}/script/"></script></div>`
            res.render('project-script',{project:getProject[0],url:url,users:req.session.user,access_level:req.session.al,nm:req.session.name,'menu':'project-script'});
        }).catch(err=>{
            res.json({status:false,message:'There is no project.'})
        });
    }else{
        res.redirect('/')
    }
}

function encrypt(id){
    var cipher = crypto.createCipher('aes-256-cbc','d6F3Efeq');
    var crypted = cipher.update(id,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
}