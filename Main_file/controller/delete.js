const commonModel = require('../models/common'); 
const customerModel = require('../models/customer'); 

async function deleteNotification(wh){
    var deleteNotification= await commonModel.deleteData('pt_notifications',wh); 
    return deleteNotification
}

async function findTicket(wh){
    var findTicket = await customerModel.selectData('id','pt_conversation_tickets',wh);
    return findTicket 
}

async function delTicket(wh){
    var ticketDel = await commonModel.deleteData('pt_conversation_tickets',wh);
    return ticketDel
}

async function deleteUsers(where){
    var userDel = await commonModel.deleteData('pt_customers',where);
    return userDel
}

module.exports= {deleteNotification, findTicket, delTicket, deleteUsers};
