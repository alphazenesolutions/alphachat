const urlParams = new URLSearchParams(window.location.search);
const myParam = urlParams.get('u-id');

$(document).ready(function(){
    const socket = io.connect();
    var customer_id = "";
    var project_id =  "";

    if(myParam != null){
        setTimeout(function(){
            $('.ptc_cu_item[u-id="'+myParam+'"]').trigger('click');
        },10);
         
    }else{
        setTimeout(function(){
            $('.ptc_cu_item:eq(0)').trigger('click');
        },100);
    }

    //new message append in real time.
    socket.on('new_message',function(data){ 
        //append notification in header in real time
        var newNot = `<div class="pt_nf_item pt_nf_new new_notify"  data-type="new">
                    <div class="pt_nf_icon">
                        <i class="far fa-comment-alt"></i>
                    </div>
                    <div class="pt_nf_detail">
                        <h3 title="`+data.message.customer_email+`" customer-id="`+data.message.customer_id+`">`+data.message.customer_name+`  <span>`+moment(data.message.time).fromNow()+`</span></h3>
                        <p project-id="`+data.message.project_id+`"><i class="far fa-dice-d6"></i> `+data.message.project_name+`</p>
                    </div>
                </div>`
        var cid = $('.pt_nf_detail').find('h3').attr('customer-id');
        $('.pt_nf_list > .no_notification').remove();
        var cid = '';
        $('.pt_nf_detail').each(function(){
            if($(this).find('h3').attr('customer-id')==data.message.customer_id){
                cid = $(this).find('h3').attr('customer-id');
            }
        })
        if(cid==data.message.customer_id){
            $('.pt_nf_item > .pt_nf_detail > h3[customer-id="'+data.message.customer_id+'"]').parents('.pt_nf_item').remove();
            $('.pt_nf_list').prepend(newNot)
        }else{
            $('.pt_nf_list').prepend(newNot)
        }
        
        $('.pt_notification_count').css('display','block');
        $('.pt_notification_count').html($('.pt_nf_new').length);
        projectNotify(data.message.project_id);
        $('.ptc_cu_item').each(function(){ 
            var rec1 = $(this).attr('u-id');
            if((rec1 == data.message.customer_id)==true){
                if($(this).hasClass('active')){
                    $.post('/projects/get-message',{customer_id:data.message.customer_id,project_id:data.message.project_id}, function(response){
                        
                    });
                    let msg =data.message.msg.replace(/\n/g,'<br>')
                    msg =convert(msg)
                    var msg1 = `<div class="ptc_cb_msg ptc_left">
                        <div class="ptc_cb_msg_inner">
                            <p class="ptc_cb_msg_time">`+data.message.customer_name+`, `+getCurrentTime()+`</p>
                            <p class="ptc_cb_msg_text">`+msg+`</p>
                        </div></div>`;
                    $('#messageBody').append(msg1);
                    resetMessageScroll();
                }else{
                    messageNotify(data.message.customer_id);
                }
                
                var result = $('.ptc_cu_item').sort(function (a, b) {
                    var contentA =parseInt($(a).attr('u-id'));
                    var contentB =parseInt($(b).attr('u-id'));
                    return (data.message.customer_id==contentA) ? -1 : (data.message.customer_id==contentB) ? 1 : 0;
                });
                $('.ptc_chat_user_list').html(result);
            }
        });
    });

    //change customer status online to offline
    socket.on('customer_offline',function(data){
        $('.ptc_cu_item').each(function(){ 
            var id = $(this).attr('u-id');
            if(id==data.customer_id){
                $(this).find('.ptc_cu_avatar > span').removeClass('online');
                $(this).find('.ptc_cu_avatar > span').addClass('offline');
                $(this).find('.ptc_cu_avatar > span').attr('title','offline');
                $('.ptc_cb_header p').html('<span class="ptc_status Offline" title="Offline"></span> Offline')
            }
        });
    });

    //change customer status offline to online
    socket.on('customer_online',function(data){
        $('.ptc_cu_item').each(function(){ 
            var id = $(this).attr('u-id');
            if(id==data.customer_id){
                $(this).find('.ptc_cu_avatar > span').removeClass('offline');
                $(this).find('.ptc_cu_avatar > span').addClass('online');
                $(this).find('.ptc_cu_avatar > span').attr('title','online');
                $('.ptc_cb_header p').html('<span class="ptc_status online" title="Online"></span> Online')
            }
        });
    });
    
    socket.on('new_customer',function(data){
        var id_match='';
        var pid
        $('.ptc_cu_item').each(function(){ 
            var id = $(this).attr('u-id');
            pid= $(this).attr('pt-pro-id');
            if(id==data.customer_id ){
                id_match='1';
            }
        });
        if(id_match=='' && pid==data.project_id){
            var n=data.customer_name.split(' ');
            var f 
            if(n.length==2){
                f=n[0][0]+n[1][0];
            }else{
                f=n[0][0]
            }
            var usrData = `<div class="ptc_cu_item" u-id="${data.customer_id}" pt-pro-id="${data.project_id}">
                <div class="ptc_cu_avatar">${f}<span class="ptc_status online" title="Online"></span></div>
                <div class="ptc_cu_detail">
                    <div class="ptc_cu_name">${data.customer_name}</div>
                    <div class="ptc_cu_time">${moment(data.time).format('LL')}&nbsp;&nbsp; &nbsp;&nbsp;<span class="ptc_chat_count_${data.customer_id}" style="display:none;"></span></div>
                </div>
            </div>`
            $('.ptc_chat_user_list').prepend(usrData);
        }
    })

    //filter customer
    $(document).on('keyup','#ptc_search_input' ,function() {
        var value = $(this).val();
        var  count = 0;
        $("#search_customer .ptc_chat_user_list .ptc_cu_item").each(function() {
            if ($(this).find('.ptc_cu_name').text().search(new RegExp(value, "i")) < 0) {
                $(this).hide(); 
                $('.ptc_chat_user_list p').css('display','');
            } else {
                $(this).show(); // MY CHANGE
                $('.ptc_chat_user_list p').css('display','none');
                count++;
            }
        });
    });

    //click on user name and find messages
    $(document).on('click','.ptc_cu_item', function(){
        $('#ptc_send_message').val('');
        $('.ptc_cu_item').removeClass('active');
        $(this).addClass('active');
        customer_id = $(this).attr('u-id');
        project_id = $(this).attr('pt-pro-id');
        var name = $(this).find('.ptc_cu_name').text();
        $('.ptc_cb_header').find('h3').text(name);
        $('.ptc_chatbox_wrapper').css('display','');
        var status = $(this).find('.ptc_cu_avatar').contents('span')[0].outerHTML;
        var title = $(this).find('.ptc_cu_avatar span').attr('title');
        var st = status +' '+ title
        $('.ptc_cb_header p').html(st);

        $.post('/projects/get-message',{customer_id:customer_id,project_id:project_id}, function(response){
            if(response.status == true){
                $('.ptc_chat_count_'+customer_id).css('display','none');
                $('.ptc_chat_count_'+customer_id).html('');
                if(response.Data.length>0){
                    var messages = JSON.stringify(response)
                    var chatMsg = JSON.parse(messages)
                    var Msg = chatMsg.Data
                    var message = "";
                    
                    for (var i = 0; i < Msg.length; i++) { 
                        let msg =Msg[i].message.replace(/\n/g,'<br>')
                        msg =convert(msg)
                        if(Msg[i].operator_id == 0){ 
                            message += `<div class="ptc_cb_msg ptc_left">
                                <div class="ptc_cb_msg_inner">
                                <p class="ptc_cb_msg_time">`+name+`, `+Msg[i].createdDate+`</p>
                                <p class="ptc_cb_msg_text">`+msg+`</p></div></div>`;    
                        } else { 
                            message += `<div class="ptc_cb_msg ptc_right">
                            <div class="ptc_cb_msg_inner">
                            <p class="ptc_cb_msg_time">`+Msg[i].name+', '+Msg[i].createdDate+`</p>
                            <p class="ptc_cb_msg_text">`+msg+`</p>
                            </div></div>`;
                        }
                    }      
                    $('#messageBody').html(message);
                    resetMessageScroll()
                }else{
                    $('#messageBody').html('');
                }
            }else{
                $('#messageBody').html('');
            }
        });
    });

    $(document).on('keyup keypress','#ptc_send_message', function(e) {
        var msg = $(this).val();
        var opName = $('#hidden_data').attr('operator-name');
        var opId = $('#hidden_data').attr('operator-id');
        if(msg){
            $('.ptc_send_message_btn').addClass('active');
        }else{
            $('.ptc_send_message_btn').removeClass('active');
        }

        var keyCode = e.keyCode || e.which;
        if (keyCode == 13) { 
            if (!event.shiftKey){
                e.preventDefault();
                if(msg){
                    socket.emit('send_message',{ customer_id:customer_id,operator_name:opName,project_id:project_id,msg:msg,time:getCurrentTime() });
                    $.post('/operators/sendMsgOperator',{customer_id:customer_id,operator_id:opId,project_id:project_id,msg:msg,time:ptCurrentDate()+' '+getCurrentTime()}, function(response){
                        
                    });
                    $.post('/operators/checkActivity',{active:1},function(response){
                        if(response.status==true){
                            if(response.operator.length>0){
                                socket.emit('send_operator',{operator_name:opName,project_id:project_id,msg:msg,time:getCurrentTime() });
                            }
                        }
                    });
                    msg = msg.replace(/\n/g,'<br>')
                    msg = convert(msg)
                    $('.ptc_cb_msg_wrapper').append(`<div class="ptc_cb_msg ptc_right">
                    <div class="ptc_cb_msg_inner">
                    <p class="ptc_cb_msg_time">`+opName+`, `+getCurrentTime()+`</p>
                    <p class="ptc_cb_msg_text">`+msg+`</p>
                    </div></div>`);
                    $('#ptc_send_message').val('');
                    resetMessageScroll();
                }
                return false;
            }
        }
    });

    socket.on('send_operator_message',function(data){ 
        var w = window.location.href.split('/');
        if(w[5]==data.message.project_id){
            let msg =data.message.msg.replace(/\n/g,'<br>');
            msg =convert(msg);
            var msg1 = `<div class="ptc_cb_msg ptc_right">
                <div class="ptc_cb_msg_inner">
                    <p class="ptc_cb_msg_time">`+data.message.operator_name+`, `+getCurrentTime()+`</p>
                    <p class="ptc_cb_msg_text">`+msg+`</p>
                </div></div>`;
            $('#messageBody').append(msg1);
            resetMessageScroll();
        }
    });        

    $(document).on('click','.ptc_send_message_btn.active',function(){
        var msg = $('#ptc_send_message').val();
        var opId = $('#hidden_data').attr('operator-id');
        if(msg){
            socket.emit('send_message',{ customer_id:customer_id,project_id:project_id,msg:msg });
            $.post('/operators/sendMsgOperator',{customer_id:customer_id,operator_id:opId,project_id:project_id,msg:msg}, function(response){
                
            });
            $.post('/operators/checkActivity',{active:1},function(response){
                if(response.status==true){
                    if(response.operator.length>0){
                        socket.emit('send_operator',{operator_name:opName,project_id:project_id,msg:msg,time:getCurrentTime() });
                    }
                }
            });
            msg = msg.replace(/\n/g,'<br>')
            msg = convert(msg)
            $('.ptc_cb_msg_wrapper').append(`<div class="ptc_cb_msg ptc_right">
            <div class="ptc_cb_msg_inner">
            <p class="ptc_cb_msg_time">`+getCurrentTime()+`</p>
            <p class="ptc_cb_msg_text">`+msg+`</p>
            </div></div>`);
            $('#ptc_send_message').val('');
            resetMessageScroll();
            $(this).removeClass('active');
        }
    });

});

function projectNotify(pid){
    var currentCount = parseInt($('.pt_pro_chat_count_'+pid).html());
    if($('.pt_pro_chat_count_'+pid).html()==''){
        $('.pt_pro_chat_count_'+pid).css('display','');
        $('.pt_pro_chat_count_'+pid).html('1');
    }else{
        $('.pt_pro_chat_count_'+pid).html(currentCount+1);
    }
}

function messageNotify(cid){
    var currentCount = parseInt($('.ptc_chat_count_'+cid).html());
    if($('.ptc_chat_count_'+cid).html()==''){
        $('.ptc_chat_count_'+cid).css('display','');
        $('.ptc_chat_count_'+cid).html('1');
    }else{
        $('.ptc_chat_count_'+cid).html(currentCount+1);
    }
}

function getCurrentTime(){
    var date = new Date()
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function ptCurrentDate(){
    var pt_dt = new Date();
    var pt_date = pt_dt.getDate(), pt_month =  pt_dt.getMonth()+1, pt_year = pt_dt.getFullYear();
    if (pt_date.toString().length == 1) {
        pt_date = "0" + pt_date;
    }

    if (pt_month.toString().length == 1) {
        pt_month = "0" + pt_month;
    }

    var pt_Current_date = pt_year+'-'+pt_month+'-'+pt_date;
    return pt_Current_date;
}

function convert(textString)
{
    var text=textString;
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    var text1=text.replace(exp,  '<a target="_blank" href="$1">$1</a>');
    var exp2 =/(^|[^\/])(www\.[\S]+(\b|$))/gim;
    return text1.replace(exp2, '$1<a target="_blank" href="http://$2">$2</a>');
}