var pt_basic_url= 'BASE_URL';
var pt_local_url= window.location.href
var pt_project_setting = {};
var socket
var pt_customer_ip='';
window.addEventListener('load', function(){
    var script = document.createElement("script");
    script.src = pt_basic_url+"/socket.io/socket.io.js";
    document.getElementsByTagName('body')[0].appendChild(script);            
    setTimeout(function(){
        window.pixeltalkId = document.getElementById("pixeltalk").getAttribute('ref');
        pixelTalkProject(pixeltalkId);
    },100)
    ptGetIPAddress()
});

function pxCallFormData(pt_cookie_name,res){ 
    socket = io.connect(pt_basic_url);
    //append widget
    var jsonData = pxWidgetScript(res.setting)
    document.getElementsByTagName("body")[0].innerHTML += jsonData;
    //set attribute in title.
    document.querySelector('.ptw_cb_title').setAttribute("p-nm",res.px_project[0].project_name);
    //append style tag in head tag.
    var styleSheet = document.createElement("style");
    styleSheet.type = "text/css"
    styleSheet.innerText = pt_css_file();
    document.head.appendChild(styleSheet);

    //click on chat widget toggle.
    var pt_chat = document.getElementsByClassName('ptw_chat_toggle');
    pt_chat[0].addEventListener("click", function(){
        var dd = document.querySelector('.ptw_chat_box_wrapper');
        if(dd.style.display == 'none'){
            dd.style.display = 'block';
            this.style.display ='none';
        }else{
            dd.style.display = 'none';
        }
        ptResetMessageScroll()
    });
    
    //get message from operator in real time.
    socket.on('send_cust_message',function(data){ 
        if(pt_cookies('pt_name')){
            var pt_customer_id= pt_cookies('customer_id');
        }else{
            var pt_customer_id = document.querySelector('.ptw_send_message_btn').getAttribute('customer-id');
        }
        if(pt_customer_id === data.message.customer_id){
            data.message.msg = data.message.msg.replace(/\n/g,'<br>')
            data.message.msg = pt_convert(data.message.msg)
            var pt_msg = `<div class="ptw_cb_msg ptw_left">
                <div class="ptw_cb_msg_inner">
                    <p class="ptw_cb_msg_time">`+data.message.operator_name+`, `+pxGetCurrentTime()+`</p>
                    <p class="ptw_cb_msg_text">`+data.message.msg+`</p>
                </div>
            </div>`;
            ptw_messageBody.innerHTML += pt_msg;
            ptResetMessageScroll()
            return true;
        }
    });

    //close chat.
    document.getElementsByClassName('ptw_cb_close_chat')[0].addEventListener('click', function(){
        if(this.getAttribute('user-activity')=='login'){
            if(confirm('Are you sure you want to close chat???')){
                document.querySelector('.ptw_chat_box').style.display = 'none';
                document.querySelector('.ptw_login_box').style.display = 'block';
                pixel_user_name.value='';
                pixel_user_email.value='';
                this.setAttribute('user-activity','');
                var pt_customerId
                if(pt_cookies('customer_id')){
                    pt_customerId=pt_cookies('customer_id')
                }else{
                    pt_customerId = document.querySelector('.ptw_send_message_btn').getAttribute('customer-id');
                }
                updateCustomer(pt_customerId);
                socket.emit('offline',{'customer_id':pt_customerId});
                pt_unset_cookies();
            }
        }else{
            document.querySelector('.ptw_chat_toggle').style.display ='';
            document.querySelector('.ptw_chat_box_wrapper').style.display = 'none';
            this.removeAttribute('user-activity');
        }
    });
    
    document.getElementsByClassName('ptw_cb_minimize_chat')[0].addEventListener('click', function(){
        document.querySelector('.ptw_chat_toggle').style.display ='';
        document.querySelector('.ptw_chat_box_wrapper').style.display = 'none';
    });

    //send message to operator on enter click.
    var pt_text = document.querySelector('.ptw_cb_textarea');
    pt_text.addEventListener('keyup', function(e){
        var pt_msg1 = this.value;
        if(pt_msg1){
            document.querySelector('.ptw_send_message_btn').classList.add('active');
            ptActiveButtonCall();
        }else{
            document.querySelector('.ptw_send_message_btn').classList.remove('active');
        }
    });
    pt_text.addEventListener('keydown', function(e){
        var pt_msg = this.value;
        
        var pt_name, pt_cid, pt_customer_id, pt_project_id, pt_project_name, pt_email
        if(pt_cookies('pt_name')){
            pt_name= pt_cookies('pt_name');
            pt_cid= pt_cookies('conversation_id');
            pt_customer_id= pt_cookies('customer_id');
            pt_project_id= pt_cookies('project_id');
            pt_project_name= pt_cookies('pt_project_name');
            pt_email= pt_cookies('pt_email');
        }else{
            pt_name = document.querySelector('.ptw_chat_box').getAttribute('customer-name');
            pt_email = document.querySelector('.ptw_chat_box').getAttribute('customer-email');
            pt_customer_id = document.querySelector('.ptw_send_message_btn').getAttribute('customer-id');
            pt_cid = document.querySelector('.ptw_send_message_btn').getAttribute('conversation-id');
            pt_project_id = document.querySelector('.ptw_send_message_btn').getAttribute('project-id');
            pt_project_name = document.querySelector('.ptw_cb_title').getAttribute('p-nm');
        }
        var keyCode = e.keyCode || e.which;
        if (keyCode == 13) { 
            if (!event.shiftKey){
                e.preventDefault();
                if(pt_msg){
                    socket.emit('message',{conversation_id:pt_cid,customer_name:pt_name,customer_email:pt_email,customer_id:pt_customer_id,project_id:pt_project_id,project_name:pt_project_name,msg:pt_msg,time:ptCurrentDate()+' '+pxGetCurrentTime()});
                    pt_msg = pt_msg.replace(/\n/g,'<br>')
                    var msg = `<div class="ptw_cb_msg ptw_right">
                        <div class="ptw_cb_msg_inner">
                            <p class="ptw_cb_msg_time">`+pt_name+`, `+pxGetCurrentTime()+`</p>
                            <p class="ptw_cb_msg_text">`+pt_convert(pt_msg)+`</p>
                        </div>
                    </div>`;              
                    ptw_messageBody.innerHTML +=msg
                    document.querySelector('.ptw_cb_textarea').value='';
                    pxSendMessage(pt_cid,pt_customer_id,pt_msg,pt_project_id,ptCurrentDate()+' '+pxGetCurrentTime())
                    ptResetMessageScroll();
                }
                return false;
            }
        }
    });
    
    //get cookies exist or not.
    if(pt_cookie_name && pt_cookies('project_id') && pt_cookies('conversation_id') && pt_cookies('customer_id')){
        var pt_pid= pt_cookies('project_id');
        var pt_customer_id= pt_cookies('customer_id');
        var pt_name= pt_cookies('pt_name');
        ptCustomerExists(pt_customer_id,pt_pid,pt_name)        
    }
}

function ptActiveButtonCall(){
    var pt_btn_active = document.getElementsByClassName('active')[0]
    pt_btn_active.addEventListener('click', function(){
        console.log('enter click')
        var msg = document.querySelector('.ptw_cb_textarea').value;
        console.log(msg)
        var pt_name, pt_cid, pt_customer_id, pt_project_id, pt_project_name, pt_email
        if(pt_cookies('pt_name')){
            pt_name= pt_cookies('pt_name');
            pt_cid= pt_cookies('conversation_id');
            pt_customer_id= pt_cookies('customer_id');
            pt_project_id= pt_cookies('project_id');
            pt_project_name= pt_cookies('pt_project_name');
            pt_email= pt_cookies('pt_email');
        }else{
            pt_name = document.querySelector('.ptw_chat_box').getAttribute('customer-name');
            pt_email = document.querySelector('.ptw_chat_box').getAttribute('customer-email');
            pt_customer_id = document.querySelector('.ptw_send_message_btn').getAttribute('customer-id');
            pt_cid = document.querySelector('.ptw_send_message_btn').getAttribute('conversation-id');
            pt_project_id = document.querySelector('.ptw_send_message_btn').getAttribute('project-id');
            pt_project_name = document.querySelector('.ptw_cb_title').getAttribute('p-nm');
        }
        if(msg){
            socket.emit('message',{conversation_id:pt_cid,customer_name:pt_name,customer_email:pt_email,customer_id:pt_customer_id,project_id:pt_project_id,project_name:pt_project_name,msg:msg,time:ptCurrentDate()+' '+pxGetCurrentTime()});
            msg = msg.replace(/\n/g,'<br>')
            var msg1 = `<div class="ptw_cb_msg ptw_right">
                <div class="ptw_cb_msg_inner">
                    <p class="ptw_cb_msg_time">`+pt_name+`, `+pxGetCurrentTime()+`</p>
                    <p class="ptw_cb_msg_text">`+pt_convert(msg)+`</p>
                </div>
            </div>`;              
            ptw_messageBody.innerHTML += msg1
            document.querySelector('.ptw_cb_textarea').value='';
            pxSendMessage(pt_cid,pt_customer_id,msg,pt_project_id,ptCurrentDate()+' '+pxGetCurrentTime())
            ptResetMessageScroll();
            this.classList.remove('active')
        }
    });
}

//find project exist or not.
function pixelTalkProject(pid){
    var xhttp= new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200 ){ 
            var res=JSON.parse(this.responseText)
            if(res.status == true){
console.log(res)

                if(res.px_project[0].status==1){
                    pt_project_setting = res.setting
                    ptGetProjectValue(res);
                }
            }
        }
    }
    var pxReqData = `id=${pid}&&url=${pt_local_url}`
    xhttp.open('post', pt_basic_url+"/pixaltalk/checkProject", true);
    xhttp.setRequestHeader('content-type','application/x-www-form-urlencoded');
    xhttp.send(pxReqData);
}

//if project exist than it will call.
function ptGetProjectValue(res){
    if(res.status==true){
        var pt_cookie_name = pt_cookies('pt_name');
        pxCallFormData(pt_cookie_name,res);
    }
}

//find customer is exists or not.
function ptCustomerExists(pt_customer_id,pt_pid,pt_name){
    var xhttp= new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200 ){ 
            var res=JSON.parse(this.responseText)
            if(res.status==true){
                ptGetAllMessage(pt_customer_id, pt_pid,pt_name);
                document.querySelector('.ptw_cb_close_chat').setAttribute('user-activity','login');
                var pt_login_box = document.querySelector('.ptw_login_box');
                pt_login_box.style.display = 'none';
                var pt_chat_box =  document.querySelector('.ptw_chat_box');
                if(pt_chat_box.style.display = 'none'){
                    pt_chat_box.style.display = 'flex';
                }else{
                    pt_chat_box.style.display = 'none';
                }
            }else{
                pt_unset_cookies()
            }
        }
    }
    
    xhttp.open('post', pt_basic_url+"/pixaltalk/find-customer", true);
    xhttp.setRequestHeader('content-type','application/x-www-form-urlencoded');
    xhttp.send('id='+pt_customer_id);
}

//store message in database. 
function pxSendMessage(pt_cid,pt_customerId,pt_msg,pt_project_id,time){
    var xhttp= new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200 ){ 
            var res=JSON.parse(this.responseText)
        }
    }

    var pxMsg = `conversation_id=${pt_cid}&&customer_id=${pt_customerId}&&message=${pt_msg}&&project_id=${pt_project_id}&&time=${time}`
    
    xhttp.open('post', pt_basic_url+"/pixaltalk/send-message", true);
    xhttp.setRequestHeader('content-type','application/x-www-form-urlencoded');
    xhttp.send(pxMsg);
}

//change activity of customer. 
function updateCustomer(pt_customerId){
    var xhttp= new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200 ){ 
            var res=JSON.parse(this.responseText)
        }
    }

    xhttp.open('post', pt_basic_url+"/pixaltalk/update-customer", true);
    xhttp.setRequestHeader('content-type','application/x-www-form-urlencoded');
    xhttp.send('customer_id='+pt_customerId);
}

function ptGetIPAddress() {
    var xhttp= new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200 ){ 
            var res=JSON.parse(this.responseText)
            pt_customer_ip = res.ip
        }
    }

    xhttp.open('get',"https://jsonip.com", true);
    xhttp.send();
};

//find customer is exist or not, if exist then it will add customer.
function pt_register_user(){
    if(pxFormValidation()==true){
        var xhttp= new XMLHttpRequest();
        xhttp.onreadystatechange = function(){
            if(this.readyState == 4 && this.status == 200 ){ 
                var res=JSON.parse(this.responseText);
                if(res.status == true){
                    var pt_id = '';
                    if(res.px_customer.insertId){
                        pt_id += res.px_customer.insertId
                    }else{
                        pt_id+=res.px_customer[0].id
                    }
                    var pt_cid = '';
                    if(res.conId.insertId){
                        pt_cid += res.conId.insertId
                    }else{
                        pt_cid+=res.conId[0].id
                    }
                    ptGetAllMessage(pt_id,res.px_project[0].id,res.px_customer[0].name);
                    pxSetCookie('1','pt_name',res.px_customer[0].name);
                    pxSetCookie('1','pt_email',res.px_customer[0].email);
                    pxSetCookie('1','customer_id',pt_id);
                    pxSetCookie('1','conversation_id',pt_cid);
                    pxSetCookie('1','project_id',res.px_project[0].id);
                    pxSetCookie('1','pt_project_name',res.px_project[0].project_name);
                    document.querySelector('.ptw_send_message_btn').setAttribute("customer-id", pt_id);
                    document.querySelector('.ptw_send_message_btn').setAttribute("conversation-id", pt_cid);
                    document.querySelector('.ptw_send_message_btn').setAttribute("project-id", res.px_project[0].id);
                    document.querySelector('.ptw_chat_box').setAttribute('customer-name',res.px_customer[0].name);
                    document.querySelector('.ptw_cb_close_chat').setAttribute('user-activity','login');
                    document.querySelector('.ptw_cb_close_chat').style.display = '';
                    var pt_login_box = document.querySelector('.ptw_login_box');
                    pt_login_box.style.display = 'none';
                    var pt_chat_box =  document.querySelector('.ptw_chat_box');
                    if(pt_chat_box.style.display = 'none'){
                        pt_chat_box.style.display = 'flex';
                    }else{
                        pt_chat_box.style.display = 'none';
                    }
                    socket.emit('online',{customer_id:pt_id});
                    socket.emit('new_user',{project_id:res.px_project[0].id,customer_id:pt_id,customer_name:res.px_customer[0].name,time:ptCurrentDate()+' '+pxGetCurrentTime()})
                }
            }
        }

        var pxFormData = `email=${pixel_user_email.value}&&name=${pixel_user_name.value}&&customer_ip=${pt_customer_ip}`
        
        xhttp.open('post', pt_basic_url+"/pixaltalk/pxCustomer/"+pixeltalkId, true)
        xhttp.setRequestHeader('content-type','application/x-www-form-urlencoded');
        xhttp.send(pxFormData);
    }else{
        pxFormValidation();
    }
}

//get all message of login user.
function ptGetAllMessage(pt_cid, pt_pid,pt_name){
    var xhttp= new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200 ){ 
            var res=JSON.parse(this.responseText);
            if(res.status == true){
                if(res.Data.length>0){
                    var pt_messages = JSON.stringify(res)
                    var pt_chatMsg = JSON.parse(pt_messages)
                    var pt_Msg = pt_chatMsg.Data
                    var pt_message = "";
                    
                    for (var i = 0; i < pt_Msg.length; i++) { 
                        let msg = pt_Msg[i].message.replace(/\n/g,'<br>')
                        msg =pt_convert(msg)
                        if(pt_Msg[i].operator_id == 0){ 
                            pt_message += `<div class="ptw_cb_msg ptw_right">
                                <div class="ptw_cb_msg_inner">
                                    <p class="ptw_cb_msg_time">`+pt_name+`, `+pt_Msg[i].createdDate+`</p>
                                    <p class="ptw_cb_msg_text">`+msg+`</p>
                                </div>
                                </div>`
                        } else { 
                            pt_message += `<div class="ptw_cb_msg ptw_left">
                                <div class="ptw_cb_msg_inner">
                                    <p class="ptw_cb_msg_time">`+pt_Msg[i].name+`, `+pt_Msg[i].createdDate+`</p>
                                    <p class="ptw_cb_msg_text">`+msg+`</p>
                                </div>
                            </div>`;
                        }
                    }      
                    if(res.message){
                        ptw_messageBody.innerHTML = pt_message + res.message;
                        ptResetMessageScroll();
                    }else{
                        ptw_messageBody.innerHTML = pt_message
                        ptResetMessageScroll();
                    }
                }else{
                    ptw_messageBody.innerHTML = '';
                }
               
            }
        }
    }

    var pxAllMsg = `customer_id=${pt_cid}&&project_id=${pt_pid}`
    
    xhttp.open('post', pt_basic_url+"/projects/get-message", true)
    xhttp.setRequestHeader('content-type','application/x-www-form-urlencoded');
    xhttp.send(pxAllMsg);
}


//function for convert text if it is link type.
function pt_convert(textString)
{
    var text=textString;
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    var text1=text.replace(exp, '<a target="_blank" href="$1">$1</a>');
    var exp2 =/(^|[^\/])(www\.[\S]+(\b|$))/gim;
    return text1.replace(exp2, '$1<a target="_blank" href="http://$2">$2</a>');
}

//function for scroll down after calling.
function ptResetMessageScroll(){
    var messageBody = document.getElementById('ptw_messageBody');
    if(messageBody){
        messageBody.scrollTop = messageBody.scrollHeight;
    }
}

//get current time.
function pxGetCurrentTime(){
    var date = new Date()
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function ptCurrentDate(){
    var pt_dt = new Date();
    var pt_date = pt_dt.getDate(), pt_month =  pt_dt.getMonth()+1, pt_year = pt_dt.getFullYear();
    if (pt_date.toString().length == 1) {
        pt_date = "0" + pt_date;
    }

    if (pt_month.toString().length == 1) {
        pt_month = "0" + pt_month;
    }

    var pt_Current_date = pt_year+'-'+pt_month+'-'+pt_date;
    return pt_Current_date;
}

// Get Cookie Name
function pt_cookies(pt_ckName) {
    var pt_ckValue = null,
    setcookies = document.cookie,
    splitArr = setcookies.split(";");
    for (var i = 0; i < splitArr.length; i++) {
        var t = splitArr[i].split("=");
        if (t.length && t.length >= 2) {
        var k = t[0].trim(),
        v = t[1].trim();
        pt_ckName == k && (pt_ckValue = v)
        }
    }
    return pt_ckValue
}

// Set Cookie
function pxSetCookie(cdays,pt_cname, pt_cvalue){
    var d = new Date();
    d.setTime(d.getTime() + (cdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = pt_cname + "=" + pt_cvalue + ";" + expires;
}

// Unset Cookie
function pt_unset_cookies() {
    now = new Date();
    now.setMonth(now.getMonth() - 1);
    var ex = now.toUTCString();
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires="+ex;
    }
}

//login form validation.
function pxFormValidation(){
    pixel_user_name_error.innerHTML = '';
    pixel_user_email_error.innerHTML = '';
    let n = 0; let e=0;

    if(pixel_user_name.value==''){
        pixel_user_name_error.innerHTML= pt_project_setting.error_message.err_msg;
        n=0;
    }else{
        if(!(/^[a-zA-Z0-9\s]*$/g.test(pixel_user_name.value))){
            pixel_user_name_error.innerHTML=pt_project_setting.error_message.err_msg;
            n=0;
        }else{
            n=1;
        }
    }

    //email validation
    if (pixel_user_email.value == ''){
        pixel_user_email_error.innerHTML= pt_project_setting.error_message.err_msg;
        e=0;
    }else if(pixel_user_email.value != ''){
        var pt_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (pixel_user_email.value == '' || !pt_filter.test(pixel_user_email.value)) {
            pixel_user_email_error.innerHTML= pt_project_setting.error_message.email_err_msg;
            e=0;
        }else{
            e=1;
        }
    }

    if(n==1 && e==1){
        return true;
    }else{
        return false;
    }
}

//widget html content.
function pxWidgetScript(setting){
    var pix_pro_form = `<div class="ptw_widget_wrapper ptw_widget_${setting.position} ptw_${setting.widget.type} ptw_${setting.theme.color_class}">
    <div class="ptw_widget_inner">
        <div class="ptw_chat_toggle">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="20px" height="20px"><path fill="#ffffff" d="M256 32C114.6 32 0 125.1 0 240c0 49.6 21.4 95 57 130.7C44.5 421.1 2.7 466 2.2 466.5c-2.2 2.3-2.8 5.7-1.5 8.7S4.8 480 8 480c66.3 0 116-31.8 140.6-51.4 32.7 12.3 69 19.4 107.4 19.4 141.4 0 256-93.1 256-208S397.4 32 256 32z"></path></svg>
            <span>${setting.widget.text}</span>
        </div>
        <div class="ptw_chat_box_wrapper" style="display:none;">
            <div class="ptw_chat_box_inner">
                <div class="ptw_cb_header">
                    <div class="ptw_cb_title">
                        <h3>P-Talk chat widget!</h3>
                        <div class="ptw_cb_title_actions">
                            <div class="ptw_cb_minimize_chat" title="Minimize"></div>
                            <div class="ptw_cb_close_chat" title="Close Chat"><span></span><span></span></div>
                        </div>
                    </div>
                </div>
                <div class="ptw_login_box">
                    <h3>${setting.login_screen_contents.heading}</h3>
                    <p>${setting.login_screen_contents.sub_heading}</p>
                    <div class="ptw_input_wrapper">
                        <label>${setting.login_screen_contents.name.label}</label>
                        <input type="text" class="ptw_input" id="pixel_user_name" placeholder="${setting.login_screen_contents.name.placeholder}">
                        <p id="pixel_user_name_error"></p>
                    </div>
                    <div class="ptw_input_wrapper">
                        <label>${setting.login_screen_contents.email.label}</label>
                        <input type="text" class="ptw_input" id="pixel_user_email" placeholder="${setting.login_screen_contents.email.placeholder}">
                        <p id="pixel_user_email_error"></p>
                    </div>
                    <button class="ptw_btn ptw_login_btn" onclick="pt_register_user()">${setting.login_screen_contents.button}</button>
                </div>
                <div class="ptw_chat_box">
                    <div class="ptw_cb_body">
                        <div class="ptw_cb_msg_wrapper" id="ptw_messageBody">
                            
                        </div>
                    </div>
                    <div class="ptw_cb_footer">
                        <textarea class="ptw_cb_textarea" placeholder="Enter your message"></textarea>
                        <span class="ptw_send_message_btn">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="14" height="14"><path fill="currentColor" d="M464 4.3L16 262.7C-7 276-4.7 309.9 19.8 320L160 378v102c0 30.2 37.8 43.3 56.7 20.3l60.7-73.8 126.4 52.2c19.1 7.9 40.7-4.2 43.8-24.7l64-417.1C515.7 10.2 487-9 464 4.3zM192 480v-88.8l54.5 22.5L192 480zm224-30.9l-206.2-85.2 199.5-235.8c4.8-5.6-2.9-13.2-8.5-8.4L145.5 337.3 32 290.5 480 32l-64 417.1z"></path></svg>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`

return pix_pro_form;
}

function pt_css_file(){
    // Your CSS as text
var styles = `@charset "UTF-8";@import url(https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap);.ptw_widget_wrapper *,.ptw_widget_wrapper ::after,.ptw_widget_wrapper ::before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.ptw_widget_wrapper ::-webkit-scrollbar{width:6px;height:6px}.ptw_widget_wrapper ::-webkit-scrollbar-track{background-color:#fff;border-radius:10px}.ptw_widget_wrapper ::-webkit-scrollbar-thumb{background-color:#f1f1f1;border-radius:10px}.ptw_widget_wrapper *{scrollbar-color:#fff #f1f1f1;scrollbar-width:thin}.ptw_widget_wrapper{position:fixed;bottom:30px;font-family:Poppins,sans-serif;font-size:14px;line-height:1.5}.ptw_widget_wrapper.ptw_widget_left{left:30px}.ptw_widget_wrapper.ptw_widget_right{right:30px}.ptw_widget_wrapper>.ptw_widget_inner{position:relative}.ptw_chat_toggle{width:60px;height:60px;border-radius:60px;background-color:#ff4f5a;box-shadow:0 0 40px rgba(0,0,0,.1);color:#fff;display:flex;align-items:center;justify-content:center;font-size:16px;position:relative;cursor:pointer;-webkit-animation:ptwchattoggle_anim .3s;-moz-animation:ptwchattoggle_anim .3s;animation:ptwchattoggle_anim .3s}@-webkit-keyframes ptwchattoggle_anim{0%{-webkit-transform:scale(.5);opacity:0}100%{-webkit-transform:scale(1);opacity:1}}@-moz-keyframes ptwchattoggle_anim{0%{-moz-transform:scale(.5);opacity:0}100%{-moz-transform:scale(1);opacity:1}}@keyframes ptwchattoggle_anim{0%{transform:scale(.5);opacity:0}100%{transform:scale(1);opacity:1}}.ptw_chat_toggle>span{display:none}.ptw_chat_toggle>.ptw_chat_bubble_arrow{content:"";position:absolute;top:60%;right:75%;display:block;border-top:20px solid transparent;border-bottom:0 solid transparent;border-right:20px solid #ff4f5a;-webkit-transform:rotate(-25deg);-moz-transform:rotate(-25deg);transform:rotate(-25deg)}.ptw_button .ptw_chat_toggle{width:190px;height:50px;border-radius:15px 15px 0 0;-webkit-animation:ptwchattogglebutton_anim .3s;-moz-animation:ptwchattogglebutton_anim .3s;animation:ptwchattogglebutton_anim .3s}@-webkit-keyframes ptwchattogglebutton_anim{0%{-webkit-transform:translateY(100%);opacity:0}100%{-webkit-transform:translateY(0);opacity:1}}@-moz-keyframes ptwchattogglebutton_anim{0%{-moz-transform:translateY(100%);opacity:0}100%{-moz-transform:translateY(0);opacity:1}}@keyframes ptwchattogglebutton_anim{0%{transform:translateY(100%);opacity:0}100%{transform:translateY(0);opacity:1}}.ptw_button .ptw_chat_toggle>span{display:inline-flex;margin-left:8px}.ptw_button .ptw_chat_toggle>.ptw_chat_bubble_arrow{display:none}.ptw_widget_wrapper.ptw_button{bottom:0}.ptw_chat_box_wrapper{position:absolute;bottom:100%;right:0;width:300px;display:none;-webkit-animation:ptwchatbox_anim .3s;-moz-animation:ptwchatbox_anim .3s;animation:ptwchatbox_anim .3s}@-webkit-keyframes ptwchatbox_anim{0%{-webkit-transform:translateY(10px);opacity:0}100%{-webkit-transform:translateY(0);opacity:1}}@-moz-keyframes ptwchatbox_anim{0%{-moz-transform:translateY(10px);opacity:0}100%{-moz-transform:translateY(0);opacity:1}}@keyframes ptwchatbox_anim{0%{transform:translateY(10px);opacity:0}100%{transform:translateY(0);opacity:1}}.ptw_button .ptw_chat_box_wrapper{bottom:30px}.ptw_chat_box_wrapper>.ptw_chat_box_inner{background-color:#fff;box-shadow:0 0 40px rgba(0,0,0,.1);border-radius:5px;position:relative;overflow:hidden}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_login_box,.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_offline_box{padding:30px}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_login_box>h3,.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_offline_box>h3{margin:0;margin-bottom:5px;font-size:16px;font-weight:600}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_login_box>p,.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_offline_box>p{margin:0;margin-bottom:30px;font-size:13px;font-weight:400}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_chat_box{display:none;flex-direction:column;position:relative;min-height:350px}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header{flex:none;padding:15px 14px 14px;position:relative;background-color:#ff4f5a}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header>.ptw_cb_title{display:flex;align-items:center;justify-content:space-between}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header>.ptw_cb_title>h3{margin:0;font-size:14px;font-weight:500;color:#fff}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header>.ptw_cb_title .ptw_cb_minimize_chat{width:15px;height:15px;display:inline-flex;align-items:center;justify-content:center;vertical-align:middle;cursor:pointer;margin-right:2px}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header>.ptw_cb_title .ptw_cb_minimize_chat::before{content:"";width:12px;height:2px;background-color:#fff;border-radius:3px}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header>.ptw_cb_title .ptw_cb_close_chat{width:15px;height:15px;display:inline-flex;align-items:center;justify-content:center;cursor:pointer;vertical-align:middle}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header>.ptw_cb_title .ptw_cb_close_chat>span{width:12px;height:3px;background-color:#fff;border-radius:5px;position:absolute;-webkit-transform:rotate(45deg);-moz-transform:rotate(45deg);transform:rotate(45deg)}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header>.ptw_cb_title .ptw_cb_close_chat>span:nth-child(2){-webkit-transform:rotate(-45deg);-moz-transform:rotate(-45deg);transform:rotate(-45deg)}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_chat_box>.ptw_cb_body{flex:1;position:relative}.ptw_widget_left .ptw_chat_box_wrapper{left:0;right:auto}.ptw_cb_msg_wrapper{position:absolute;top:0;left:0;right:0;bottom:0;overflow:auto;padding:15px}.ptw_cb_msg_wrapper>.ptw_cb_msg{margin-bottom:15px;-webkit-animation:ptcmsg_anim .3s;-moz-animation:ptcmsg_anim .3s;animation:ptcmsg_anim .3s;-webkit-animation-fill-mode:forwards;-moz-animation-fill-mode:forwards;animation-fill-mode:forwards}.ptw_cb_msg_wrapper>.ptw_cb_msg>.ptw_cb_msg_inner{display:inline-block;max-width:70%}.ptw_cb_msg_wrapper>.ptw_cb_msg>.ptw_cb_msg_inner>.ptw_cb_msg_text{background-color:#f1f1f1;color:#222;padding:10px 12px;border-radius:10px 10px 10px 0;font-size:13px;margin:0;word-break:break-word}.ptw_cb_msg_wrapper>.ptw_cb_msg>.ptw_cb_msg_inner>.ptw_cb_msg_text a{color:inherit;text-decoration:underline}.ptw_cb_msg_wrapper>.ptw_cb_msg.ptw_right>.ptw_cb_msg_inner>.ptw_cb_msg_text a{color:#fff;text-decoration:underline}@-webkit-keyframes ptcmsg_anim{0%{-webkit-transform:translateY(10px);opacity:0}100%{-webkit-transform:translateY(0);opacity:1}}@-moz-keyframes ptcmsg_anim{0%{-moz-transform:translateY(10px);opacity:0}100%{-moz-transform:translateY(0);opacity:1}}@keyframes ptcmsg_anim{0%{transform:translateY(10px);opacity:0}100%{transform:translateY(0);opacity:1}}.ptw_cb_msg_wrapper>.ptw_cb_msg>.ptw_cb_msg_inner>.ptw_cb_msg_time{font-size:11px;margin:0;margin-bottom:2px}.ptw_cb_msg_wrapper>.ptw_cb_msg.ptw_right{text-align:right}.ptw_cb_msg_wrapper>.ptw_cb_msg.ptw_right>.ptw_cb_msg_inner>.ptw_cb_msg_text{background-color:#ff4f5a;color:#fff;border-radius:10px 10px 0 10px;margin:0;display:inline-block;text-align:left}.ptw_cb_msg_wrapper>.ptw_cb_msg.ptw_right>.ptw_cb_msg_inner>.ptw_cb_msg_time{padding-right:5px}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_chat_box>.ptw_cb_footer{flex:none;background-color:#fff;border-top:1px solid #f1f1f1;position:relative}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_chat_box>.ptw_cb_footer>textarea.ptw_cb_textarea{width:100%;border:none;outline:0;box-shadow:none;padding:10px;padding-right:45px;height:50px;font-size:14px;background-color:transparent;resize:none;font-family:inherit}.ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_chat_box>.ptw_cb_footer>textarea.ptw_cb_textarea::-webkit-input-placeholder{color:#ccc}.ptw_cb_footer>.ptw_send_message_btn{position:absolute;top:13px;right:10px;width:30px;height:30px;background-color:#f1f1f1;border-radius:50px;display:flex;align-items:center;justify-content:center;color:#b1b1b1;padding-right:3px;cursor:pointer}.ptw_cb_footer>.ptw_send_message_btn.active{background-color:#ff4f5a;color:#fff}.ptw_input_wrapper{margin-bottom:20px}.ptw_input_wrapper>label{font-size:13px;color:#888;margin:0;font-weight:400;display:block;margin-bottom:4px}.ptw_input_wrapper>.ptw_input{width:100%;border:1px solid #e0e0e0;padding:10px;border-radius:5px;outline:0}.ptw_input_wrapper>.ptw_input:focus{border-color:#ff4f5a}.ptw_input_wrapper>textarea.ptw_input{resize:none}.ptw_input_wrapper>#pixel_user_email_error,.ptw_input_wrapper>#pixel_user_name_error{margin:0;font-size:12px;color:#f44336;line-height:inherit;margin-top:2px}.ptw_btn{display:inline-block;padding:10px 15px;border-radius:5px;background-color:#ff4f5a;color:#fff;border:none;box-shadow:none;cursor:pointer;outline:0}.ptw_t1 .ptw_btn,.ptw_t1 .ptw_cb_footer>.ptw_send_message_btn.active,.ptw_t1 .ptw_cb_msg_wrapper>.ptw_cb_msg.ptw_right>.ptw_cb_msg_inner>.ptw_cb_msg_text,.ptw_t1 .ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header,.ptw_t1 .ptw_chat_toggle{background-color:#ff4f5a}.ptw_t1 .ptw_chat_toggle>.ptw_chat_bubble_arrow{border-right-color:#ff4f5a}.ptw_t1 .ptw_input_wrapper>.ptw_input:focus{border-color:#ff4f5a}.ptw_t2 .ptw_btn,.ptw_t2 .ptw_cb_footer>.ptw_send_message_btn.active,.ptw_t2 .ptw_cb_msg_wrapper>.ptw_cb_msg.ptw_right>.ptw_cb_msg_inner>.ptw_cb_msg_text,.ptw_t2 .ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header,.ptw_t2 .ptw_chat_toggle{background-color:#9c27b0}.ptw_t2 .ptw_chat_toggle>.ptw_chat_bubble_arrow{border-right-color:#9c27b0}.ptw_t2 .ptw_input_wrapper>.ptw_input:focus{border-color:#9c27b0}.ptw_t3 .ptw_btn,.ptw_t3 .ptw_cb_footer>.ptw_send_message_btn.active,.ptw_t3 .ptw_cb_msg_wrapper>.ptw_cb_msg.ptw_right>.ptw_cb_msg_inner>.ptw_cb_msg_text,.ptw_t3 .ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header,.ptw_t3 .ptw_chat_toggle{background-color:#673ab7}.ptw_t3 .ptw_chat_toggle>.ptw_chat_bubble_arrow{border-right-color:#673ab7}.ptw_t3 .ptw_input_wrapper>.ptw_input:focus{border-color:#673ab7}.ptw_t4 .ptw_btn,.ptw_t4 .ptw_cb_footer>.ptw_send_message_btn.active,.ptw_t4 .ptw_cb_msg_wrapper>.ptw_cb_msg.ptw_right>.ptw_cb_msg_inner>.ptw_cb_msg_text,.ptw_t4 .ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header,.ptw_t4 .ptw_chat_toggle{background-color:#3f51b5}.ptw_t4 .ptw_chat_toggle>.ptw_chat_bubble_arrow{border-right-color:#3f51b5}.ptw_t4 .ptw_input_wrapper>.ptw_input:focus{border-color:#3f51b5}.ptw_t5 .ptw_btn,.ptw_t5 .ptw_cb_footer>.ptw_send_message_btn.active,.ptw_t5 .ptw_cb_msg_wrapper>.ptw_cb_msg.ptw_right>.ptw_cb_msg_inner>.ptw_cb_msg_text,.ptw_t5 .ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header,.ptw_t5 .ptw_chat_toggle{background-color:#2196f3}.ptw_t5 .ptw_chat_toggle>.ptw_chat_bubble_arrow{border-right-color:#2196f3}.ptw_t5 .ptw_input_wrapper>.ptw_input:focus{border-color:#2196f3}.ptw_t6 .ptw_btn,.ptw_t6 .ptw_cb_footer>.ptw_send_message_btn.active,.ptw_t6 .ptw_cb_msg_wrapper>.ptw_cb_msg.ptw_right>.ptw_cb_msg_inner>.ptw_cb_msg_text,.ptw_t6 .ptw_chat_box_wrapper>.ptw_chat_box_inner>.ptw_cb_header,.ptw_t6 .ptw_chat_toggle{background-color:#ff9800}.ptw_t6 .ptw_chat_toggle>.ptw_chat_bubble_arrow{border-right-color:#ff9800}.ptw_t6 .ptw_input_wrapper>.ptw_input:focus{border-color:#ff9800}`

    return styles;
}