$(document).ready(function(){
    $('.ptw_login_btn').on('click', function(){
        $(this).closest('.ptw_login_box').hide();
        $(this).closest('.ptw_chat_box_inner').find('.ptw_chat_box').css('display', 'flex');
    });
    
    $('.ptw_cb_close_chat').on('click', function(){
        $(this).closest('.ptw_chat_box_inner').find('.ptw_chat_box').css('display', 'none');
        $(this).closest('.ptw_chat_box_inner').find('.ptw_login_box').show();
    });
    
    $('.ptw_chat_toggle').on('click', function(){
        $(this).closest('.ptw_widget_inner').find('.ptw_chat_box_wrapper').toggle();
    });
});

function resetMessageScroll(){
    var messageBody = document.querySelector('#ptw_messageBody');
    if(messageBody){
        messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
    }
}