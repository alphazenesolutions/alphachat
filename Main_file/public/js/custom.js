function login(){
    if(loginValidation()==true){ 
        var xhttp= new XMLHttpRequest();
        xhttp.onreadystatechange = function(){
            if(this.readyState == 4 && this.status == 200 ){ 
                var res=JSON.parse(this.responseText)
                if(res.status == true){ 
                    showPopup('success', res.message);
                    setTimeout(function(){
                        window.location.href = '/dashboard/'
                    },500)
                }else{
                    showPopup('error', res.message);
                }
            }
        }

        var remember_me = null; 
        var inputElements = document.getElementsByName('_remember_me');
        for(var i=0; inputElements[i]; ++i){
            if(inputElements[i].checked){
                remember_me = inputElements[i].value;
                break;
            }
        }
        var requestData = `email=${pt_email.value}&&password=${pt_password.value}&&remember_me=${remember_me}`
        
        xhttp.open('post', "/", true)
        xhttp.setRequestHeader('content-type','application/x-www-form-urlencoded')
        xhttp.send(requestData);
    }else{
        loginValidation();
    }
}

function loginValidation(){
    var name = 0; var password1=0;

    //email validation
    if (pt_email.value == ''){ 
        var text = "Please fill all the fields.";
            showPopup('error', text);
            pt_email.focus();
            name=0;
    }else if(pt_email.value != ''){
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (pt_email.value == '' || !filter.test(pt_email.value)) {
            var text = "Please fill valid email.";
            showPopup('error', text);
            pt_email.focus();
            name=0;
        }else{
            name=1;
        }
    }

    //password validation
    if (pt_password.value == '') { 
        var text = "Please fill all the fields.";
        showPopup('error', text);
        pt_password.focus();
        password1 = 0;
    }else if(pt_password.value != ''){ 
        if(pt_password.value.length < 6) {
            var text = "Password is too short."
            showPopup('error', text);
            pt_password.focus();
            password1 = 0;
        }else{
           password1 = 1;
        }
    }

    if(name==1 && password1==1){
        return true;
    }else{
        return false;
    }
}

$('.input input').keypress(function(event){
    if(event.keyCode === 13){
        $('.pt_btn_dark').click();
    }
});

function forgot(){
    if(forgotPassword()==true){ 
        var pass = ''; 
        var str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +  
                'abcdefghijklmnopqrstuvwxyz0123456789@#$'; 
            
        for (i = 1; i <= 8; i++) { 
            var char = Math.floor(Math.random() 
                        * str.length + 1); 
                
            pass += str.charAt(char) 
        } 

        var xhttp= new XMLHttpRequest();
            xhttp.onreadystatechange = function(){
                if(this.readyState == 4 && this.status == 200 ){ 
                    var res=JSON.parse(this.responseText)
                    if(res.status == true){ 
                        showPopup('success', res.message);
                        setTimeout(function(){
                            window.location.href = '/'
                        },1500)
                    }else{
                        showPopup('error', res.message);
                    }
                }
            }
        
        var requestData = `email=${pt_forgot_email.value}&&newPassword=${pass}`
            
        xhttp.open('post',"/forgot-password", true)
        xhttp.setRequestHeader('content-type','application/x-www-form-urlencoded')
        xhttp.send(requestData);
    }else{
        forgotPassword();
    }
}

function forgotPassword() {
    if (pt_forgot_email.value == ''){ 
        var text = "Please fill all the fields.";
        showPopup('error', text);
        pt_forgot_email.focus();
        return false;
    }else if(pt_forgot_email.value != ''){
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (pt_forgot_email.value == '' || !filter.test(pt_forgot_email.value)) {
            var text = "Please fill valid email.";
            showPopup('error', text);
            pt_forgot_email.focus();
            return false;
        }else{
            return true;
        }
    }
}
