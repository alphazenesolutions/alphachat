var origin = window.location.origin

$(document).ready(function(){
    $.get('/projects/notification-get',function(response){
        if(response.status==true){
            var notify= response.notification
            if(notify.length>0){ 
                for(let i in notify){ 
                    if(notify[i].msg_read==0){ 
                        var nData =`<div class="pt_nf_item pt_nf_new new_notify" data-type="new"><div class="pt_nf_icon"><i class="far fa-comment-alt"></i></div><div class="pt_nf_detail"><h3 title="`+notify[i].cn.email+`" customer-id="`+notify[i].cn.id+`">`+notify[i].cn.name+`<span>`+notify[i].updatedAt+`</span></h3><p project-id="`+notify[i].pn.id+`"><i class="far fa-dice-d6"></i> `+notify[i].pn.project_name+`</p></div></div>`
                        $('.pt_nf_list').append(nData);
                    }else{
                        var nData =`<div class="pt_nf_item pt_nf_read new_notify" data-type="old"><div class="pt_nf_icon"><i class="far fa-comment-alt"></i></div><div class="pt_nf_detail"><h3 title="`+notify[i].cn.email+`" customer-id="`+notify[i].cn.id+`">`+notify[i].cn.name+`<span>`+notify[i].updatedAt+`</span></h3><p project-id="`+notify[i].pn.id+`"><i class="far fa-dice-d6"></i> `+notify[i].pn.project_name+`</p></div></div>`
                        $('.pt_nf_list').append(nData);
                    }
                } 
            }else{ 
                var nData = `<p class="no_notification">There is no notification</p>`
                $('.pt_nf_list').append(nData);
            }
            var newNotify = $('.pt_nf_new').length;
            if(newNotify!=0){
                $('.pt_notification_count').css('display','');
                $('.pt_notification_count').html(newNotify);
            }
        }
    });

    setTimeout(function(){
        $('.select_project').trigger('change');
    },10);

    //project wise data show in dashboard.
    $(document).on('change','.select_project',function(){
        var id = $(this).val();
        if(id==''){
            id = 0
        }
        loading(1)
        $.get('/dashboard/get-customer/'+id,function(response){
            if(response.status==true){
                loading(0)
                $('.pt_active_customer').html(response.activeCustomer.active)
                $('.pt_total_customer').html(response.customer.total_customer)
            }
        });
    });

    //create project step-1
    $(document).on('click','.wid_btn', function(){
        var projectName = $('#project_name').val();
        var url = $('#web_url').val();
        loading(1)
        if(newProjectValidation()==true){
            $.post('/projects/create-project',{project_name:projectName,url:url},function(response){
                loading(0)
                if(response.status==true){
                    var a = $('.wid_btn').attr('data-step');
                    $('.pt_step_wrapper').removeClass('active');
                    $('.wid_btn').closest('.pt_step_wrapper_list').find('.step'+a).addClass('active');
                    $('.wid_cont_btn').attr('pro-id',response.data.insertId);
                    $('#enc_id').val(response.encryptId);
                    showPopup('success', response.message);
                }else{
                    showPopup('error', response.message);
                }
            });
        }else{
            loading(0)
            newProjectValidation();
        }
    }); 
   
    //chat widget type
    $(document).on('change','[name="widget_type"]', function(){
        var ww = $('.ptw_widget_wrapper');
        var wt = $(this).val();
        $('[name="widget_type"]').removeAttr('checked');
        $(this).attr('checked',true);
        if (wt == 'bubble') {
            ww.addClass('ptw_bubble');
        } else {
            ww.removeClass('ptw_bubble');
        }
        if (wt == 'button') {
            ww.addClass('ptw_button');
            $('.pt_input_wt_button').removeClass('d-none');
        } else {
            ww.removeClass('ptw_button');
            $('.pt_input_wt_button').addClass('d-none');
        }
    });

    $(document).on('input','[name="widget_button_text"]',function(){
        var text = $(this).val();
        var btn = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="20px" height="20px"><path fill="#ffffff" d="M256 32C114.6 32 0 125.1 0 240c0 49.6 21.4 95 57 130.7C44.5 421.1 2.7 466 2.2 466.5c-2.2 2.3-2.8 5.7-1.5 8.7S4.8 480 8 480c66.3 0 116-31.8 140.6-51.4 32.7 12.3 69 19.4 107.4 19.4 141.4 0 256-93.1 256-208S397.4 32 256 32z"></path></svg> '+text+'</span>'
        $('.button').html(btn);
    });

    //chat widget position
    $(document).on('change','[name="widget_position"]', function(){ 
        $('[name="widget_position"]').removeAttr('checked');
        $(this).attr('checked',true);
        var wp = $(this).val();
        var ww = $('.ptw_widget_wrapper');
        if (wp == 'left') {
            ww.addClass('ptw_widget_left');
        } else {
            ww.removeClass('ptw_widget_left');
        }
        if (wp == 'right') {
            ww.addClass('ptw_widget_right');
        } else {
            ww.removeClass('ptw_widget_right');
        }
    });
    
    //create project step-2 back btn
    $(document).on('click','.wid_back_btn', function(){
        var a = $(this).attr('data-step');
        $('.pt_step_wrapper').removeClass('active');
        $(this).closest('.pt_step_wrapper_list').find('.step'+a).addClass('active');
    });

    //create project step-2 continue btn
    $(document).on('click','.wid_cont_btn', function(){
        var id = $(this).attr('pro-id');
        var widget_type_id = $('.pt_cpwt_item').find('input:checked').attr('value');
        var pos_checked_id = $('.pt_radio').find('input:checked').attr('value');
        loading(1)
        $.post('/projects/create-project',{ widget_type: widget_type_id,pos_checked:pos_checked_id,id:id},function(response){
            loading(0)
            if(response.status==true){
                showPopup('success', response.message);
                var a = $('.wid_cont_btn').attr('data-step'); 
                $('.pt_step_wrapper').removeClass('active');
                $('.wid_cont_btn').closest('.pt_step_wrapper_list').find('.step'+a).addClass('active');  
                var script = '<div id="pixeltalk" ref="'+response.enc_data+'"><script src="'+origin+'/script/"></script></div>'
                $('.pt_script_box .pro_sc').find('textarea').text(script);  
                $('.wid_finish_btn').attr('project-id',id);    
            }else{
                showPopup('error', response.message);
            }
        });
    });

    //create project step-3 back btn
    $(document).on('click','.wid_back1_btn', function(){
        var a = $(this).attr('data-step');
        $('.pt_step_wrapper').removeClass('active');
        $(this).closest('.pt_step_wrapper_list').find('.step'+a).addClass('active');
    });

    //update project name 
    $(document).on('click','.updt_project',function(){
        var id = $(this).attr('p-id');
        var project_name = $('#edit_project_name').val();
        $.post('/projects/project-update',{project_name:project_name,id:id},function(response){
            if(response.status==true){
                showPopup('success', response.message);
                loading(1);
                setTimeout(function(){
                    window.location.href='/projects';
                },1500);
            }else{
                showPopup('error', response.message);
            }
        })
    });

    //favourite click
    $(document).on('click','.pt_pro_favourite',function(){
        var id = $(this).attr('p-id');
        var message
        if($(this).parents('.pt_project').hasClass('favourite')){
            $(this).parents('.pt_project').removeClass('favourite');
            message = 'Project removed to favourite.'
        }else{
            $(this).parents('.pt_project').addClass('favourite');
            message = 'Project added to favourite.'
        }
        $.post('/projects/updateFav',{id:id},function(response){
            if(response.status==true){
                showPopup('success', message);
                loading(1)
                setTimeout(function(){
                    window.location.href='/projects';
                },1500);
            }else{
                showPopup('error', response.message);
            }
        });
    });

    //copy clipboard
    $(document).on('click','.cpy_sc',function(event){
        var $tempElement = $("<input>");
        $("body").append($tempElement);
        $tempElement.val($(this).siblings('.pro_sc').find('textarea').text()).select();
        document.execCommand("Copy");
        $tempElement.remove();
    });

    //chat open after click on notification 
    $(document).on('click','.new_notify',function(){
        var customer_id = $(this).find('h3').attr('customer-id');
        var project_id = $(this).find('p').attr('project-id');
        var cc = $(this).attr('data-type') 
        window.location.href='/projects/notification/'+project_id+'?customer_id='+customer_id+'&notify_type='+cc
    });
    
    //chat open after project_customer view 
    $(document).on('click','.customer_chat_open',function(){
        var customer_id = $(this).attr('cust-id');
        var project_id = $('.pt_ps_title').find('h3').attr('project-id');
        window.location.href='/projects/notification/'+project_id+'?customer_id='+customer_id
    });

    //filter project
    $(document).on('keyup','#pt_project_search' ,function() {
        var value = $(this).val();
        var  count = 0;
        $("#filter .pt_project_list .pt_project").each(function() {
            if ($(this).find('.pt_project_inner > .pt_pro_details > h3').text().search(new RegExp(value, "i")) < 0) {
                $(this).hide();
            } else {
                $(this).show();
                count++;
            }

            $('.pt_fav .fav').css('display','none');
            if($(".pt_fav .favourite:visible").length==0){
                $('.pt_fav .fav').css('display','');
            }

            $('.pt_apl .all_project').css('display','none');
            if($(".pt_apl .pt_all_project:visible").length==0){
                $('.pt_apl .all_project').css('display','');
            }

        });
    });    

    //oninput widget appearance settings.
    $(document).on('input','.pt_input',function(){
        var name = $(this).attr('data-name');
        var value = $(this).val();
        if($('input[name="'+name+'"]').html()==undefined){
            if($('textarea[name="'+name+'"]').val()==''){
                $('textarea[name="'+name+'"]').attr('placeholder',value);
            }
            if($('label[name="'+name+'"]').html()==undefined){
                if($('h3[name="'+name+'"]').html()==undefined){
                    if($('p[name="'+name+'"]').html()==undefined){
                        $('button[name="'+name+'"]').html(value)
                    }else{
                        $('p[name="'+name+'"]').html(value);
                    }
                }else{
                    $('h3[name="'+name+'"]').html(value);
                }
            }else{
                $('label[name="'+name+'"]').html(value);
            }
        }else{
            $('input[name="'+name+'"]').val(value);
        }
    });
    
    //widget appearance settings save.
    $(document).on('click','.pt_save_settings',function(){
        var id = $('.pt_ps_content').attr('pt-project-id');
        var color = $('.pt_color.active').attr('data-theme');
        $('input[name="theme_color"]').attr('value',color);
        loading(1);
        if(ptWidgetAppearance('pt_wa_setting')==true){
            $.post('/projects/widget-setting/'+id,$("#pt_wa_setting").serialize(),function(response){
                loading(0);
                if(response.status==true){
                    showPopup('success', response.message);
                }else{
                    showPopup('error', response.message);
                }
            });
        }else{
            loading(0);
            ptWidgetAppearance('pt_wa_setting')
        }
    });

    //project status setting
    $(document).on('click','#widget_status_chk', function(){
        var status = $(this).attr('data-status');
        var id = $(this).parents('.pt_ps_content').attr('pt-project-id');
        var ss = '';
        if(status==1){
            $(this).attr('data-status','0');
            ss+='0'
        }else{
            $(this).attr('data-status','1');
            ss+='1'
        }
        loading(1);
        $.post('/projects/project-status',{status:ss,id:id},function(response){
            loading(0);
            if(response.status==true){
                showPopup('success', response.message);
            }else{
                showPopup('error', response.message);
            }
        });
    });

    //widget auto hide setting.
    $('#auto_hide_widget').on('change', function(e){
        if(e.target.checked){
            $('.pt_wah_date').show();
        }else{
            $('.pt_wah_date').hide();
            var id = $(this).parents('.pt_ps_content').attr('pt-project-id');
            var date = '0000-00-00'
            loading(1);
            $.post('/projects/expires-project',{date:date,id:id},function(response){
                loading(0);
                if(response.status==true){
                    var message = 'auto expires updated.'
                    showPopup('success', message);
                    $('.pt_datetimepicker').val('');
                }else{
                    showPopup('error', response.message);
                }
            });
        }
    });

    //project expires setting.
    $(document).on('click','.pt_expires_project',function(){
        var id = $(this).parents('.pt_ps_content').attr('pt-project-id');
        if($('.pt_datetimepicker').val()==''){
            $('#datetimeError').html('Please select Date.')
        }else{
            var date = $('.pt_datetimepicker').val();
            loading(1);
            $.post('/projects/expires-project',{date:date,id:id},function(response){
                loading(0);
                if(response.status==true){
                    showPopup('success', response.message);
                }else{
                    showPopup('error', response.message);
                }
            });
        }
    });

    //project delete 
    $(document).on('click','.project_delete', function(){
        var project_id = $(this).attr('project-id');
        $('.pt_confirm_button').attr('project-id',project_id)
        confirm('Are you sure?', 'You want to delete project.', 'Delete Now', function(){
            $('.pt_confirm_button').click();
        });
    });

    //common delete popup
    $(document).on('click','.pt_confirm_button',function(){
        var project_id = $(this).attr('project-id');
        var op_id = $(this).attr('op-id');
        var customer_id = $(this).attr('customer-id');
        if(op_id){
            loading(1);
            $.post('/operators/delete-operator',{id:op_id},function(response){
                if(response.status==true){
                    showPopup('success', response.message);
                    setTimeout(() => {
                        window.location.href='/operators/'; 
                    },1500);
                    $('.delete_popup').click();
                }else{
                    showPopup('error', response.message);
                }
            });
        }else if(customer_id){
            loading(1);
            var p_id = $('.pt_ps_title > h3').attr('project-id');
            $.post('/projects/delete-customer',{id:customer_id},function(response){
                if(response.status==true){
                    showPopup('success', response.message);
                    $('.delete_popup').click();
                    setTimeout(() => {
                        window.location.href='/projects/project-customers/'+p_id; 
                    },1500);
                }else{
                    showPopup('error', response.message);
                }
            });
        }else{
            $.post('/projects/delete-project',{id:project_id},function(response){
                if(response.status==true){
                    showPopup('success', response.message);
                    loading(1);
                    setTimeout(() => {
                    window.location.href='/projects'; 
                    }, 200);
                }else{
                    showPopup('error', response.message);
                }
            });
        }
    });

    $(document).on('click','.delete_popup',function(){
        $('.pt_confirm_button').removeAttr('project-id')
        $('.pt_confirm_button').removeAttr('op-id')
        $('.pt_confirm_button').removeAttr('customer-id');
        $('.customer_delete').parents('tr').removeClass('pt_customer_del'); 
        $('.operator_delete').parents('tr').removeClass('pt_operator_del');

    });

    //add operator 
    $(document).on('click','.pt_op_add_btn',function(){
        var name = $('#pt_op_name').val();
        var email = $('#pt_op_email').val();
        var password = $('#pt_op_password').val();
        var project = ($('#pt_op_add_project').val()).toString();
        if(ptWidgetAppearance('add_operator_popup')==true){
            $.post('/operators/',{name:name,email:email,password:password,project:project},function(response){
                console.log(response)
		if(response.status==true){
                    showPopup('success', response.message);
                    $('.op_popup_close').click();
                    setTimeout(function(){
                        window.location.href='/operators/'
                    },100);
                }else{
                    showPopup('error', response.message);
                }
            })
        }else{
            loading(0);
            ptWidgetAppearance('add_operator_popup')
        }    
    });

    //popup close
    $(document).on('click','.op_popup_close',function(){
        $('.add_operator_popup').css('display','none');
        $('#pt_op_name').val('');
        $('#pt_op_email').val('');
        $('#pt_op_password').val('');
        $('#pt_op_add_project').val('');
    });

    //edit operator 
    $(document).on('click','.edit_btn',function(){
        $('#edit_operator_popup').modal('show');
        var id = $(this).attr('op-id');
        loading(1);
        $.get('/operators/operator-edit',{id:id},function(resOperator){
            loading(0);
            if(resOperator.status==true){
                let opData = resOperator.operator[0]
                let splitName = opData.project_id.split(',');
                $('#pt_op_edit_project').val(splitName);
                $('#pt_edit_op_name').val(opData.name);
                $('#pt_edit_op_email').val(opData.email);
                $('.pt_op_updt_btn').attr('operator-id',opData.id)
            }
        });
    });

    //update Operator
    $(document).on('click','.pt_op_updt_btn',function(){
        var id = $(this).attr('operator-id');
        var name = $('#pt_edit_op_name').val();
        var email = $('#pt_edit_op_email').val();
        var password = $('#pt_edit_op_password').val();
        var project_id = ($('#pt_op_edit_project').val()).toString();
        loading(1);
        if(ptWidgetAppearance('edit_operator_popup')==true){
            $.post('/operators/update-operator',{id:id,name:name,email:email,password:password,project_id:project_id},function(response){
                if(response.status==true){
                    showPopup('success', response.message);
                    $('.op_edit_popup_close').click();
                    setTimeout(function(){
                        window.location.href='/operators/'
                    },100);
                }else{
                    showPopup('error', response.message);
                }
            });
        }else{
            loading(0);
            ptWidgetAppearance('edit_operator_popup');
        }
    });

    //edit popup close
    $(document).on('click','.op_edit_popup_close',function(){
        $('.edit_operator_popup').css('display','none');
    });

    //Operator delete
    $(document).on('click','.operator_delete',function(){
        var operator_id = $(this).attr('op-id');
        $('.pt_confirm_button').attr('op-id',operator_id);
        $(this).parents('tr').addClass('pt_operator_del');
        confirm('Are you sure?', 'You want to delete this operator.', 'Delete Now', function(){
            $('.pt_confirm_button').click();
        });  
    }); 

    //profile update.
    $(document).on('click','.pt_profile_updt',function(){
        var fname = $('#pro_fname').val();
        var lname = $('#pro_lname').val();
        var password = $('[name="user_new_pwd"]').val();
        var c_password = $('[name="user_cnf_new_pwd"]').val();
        var id = $('#pt_profile').attr('user-id');
        loading(1);
        if(ptWidgetAppearance('pt_profile')==true){
            if(password==c_password){
                $.post('/profile/update-profile',{id:id,fname:fname,lname:lname,password:password},function(response){
                    loading(0);
                    if(response.status==true){
                        showPopup('success', response.message);
                        $('#pro_fname').val(fname);
                        $('#pro_lname').val(lname);
                        $('[name="user_new_pwd"]').val('');
                        $('[name="user_cnf_new_pwd"]').val('');
                        $('.pt_avatar_icon span').html(fname[0]+lname[0])
                        $('.pt_avatar_name').html('Hi, '+fname+' '+lname)
                    }else{
                        showPopup('error', response.message);
                    }
                });
            }else{
                showPopup('error', 'Password not match');
            }
        }else{
            loading(0);
            ptWidgetAppearance('pt_profile')
        }
    });

    //customer delete 
    $(document).on('click','.customer_delete', function(){
        var customer_id = $(this).attr('cust-id');
        $('.pt_confirm_button').attr('customer-id',customer_id)
        $(this).parents('tr').addClass('pt_customer_del');           
        confirm('Are you sure?', 'You want to delete this customer.', 'Delete Now', function(){
            $('.pt_confirm_button').click();
        });
    });
    
    // testing
    $(document).on('click','.test_btn',function(){
        $.post('/operators/testing-mail',function(response){
           console.log(response) 
        });
        
    });
});

function ptSearchProject(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
    if (myArray[i].name === nameKey) {
    return myArray[i];
    }
    }
}

function newProjectValidation(){
    var pj=0; var wb=0; 
    if(project_name.value==''){
        var text = "This field is required."
        project_name.focus();
        showPopup('error', text);
        pj=0;
    }else{
        if(!(/^[a-zA-Z0-9\s]*$/g.test(project_name.value))){
            var text = "Please fill correct name."
            project_name.focus();
            showPopup('error', text);
            pj=0;
        }else{
            pj=1;
        }
    }

    if(web_url.value==''){
        var text = "This field is required."
        showPopup('error', text);
        web_url.focus();
        wb=0;
    }else{
        var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
        if(!(regexp.test(web_url.value))){
            var text = "Please fill correct URL."
            showPopup('error', text);
            web_url.focus();
            wb=0;
        }else{
            wb=1;
        }
    }

    if(pj && wb){
        return true;
    }else{
        return false;
    }
}

function ptWidgetAppearance(id){
    let allAreFilled = true;
    document.getElementById(id).querySelectorAll(".pt_input").forEach(function(i) {
        if (!allAreFilled) return;
        if (!i.value) allAreFilled = false;
        if(i.type=='email'){
            var email = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;
            if(!email.test(i.value)){
                allAreFilled = false;
            }
        }
        if(i.type=='password'){
            if(id=='edit_operator_popup'){
                if(i.value==''){
                    allAreFilled = true;
                }else{
                    if(i.value.length < 6) {
                        allAreFilled = false;
                    }
                }
            }else{
                if(i.value.length < 6) {
                    allAreFilled = false;
                }
            }
        }
        if (!allAreFilled) {
            i.focus();
            if(i.type=='email'){
				if(i.value==''){
					showPopup('error', 'Please fill all the fields.');
				}else{
                	showPopup('error', 'Please fill valid email.');
				}
            }else if(i.type=='password'){
				if(i.value==''){
					showPopup('error', 'Please fill all the fields.');
				}else{
                	showPopup('error', 'Password is too short.');
				}    
            }else{
                showPopup('error', 'Please fill all the fields.');
            }
        }
    });
    return allAreFilled;
}