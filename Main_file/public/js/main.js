$(document).ready(function(){
    if ($('[data-toggle="tooltip"]').length) {
        $('[data-toggle="tooltip"]').tooltip();
    }
    
    if($('.pt_datatable').length){
        $('#data_table').DataTable({
            responsive: true
        });
        $('#customer_datatable').DataTable({
            responsive: true
        });
    }
    resetMessageScroll();
    if($('#ptc_send_message').length){
    }

    if($('.pt_wa_preview').length){
        var ww = $('.ptw_widget_wrapper');
        /* change widget color start */
        $('.pt_color').on('click', function(){
            $('.pt_color').removeClass('active');
            ww.removeClass('ptw_t1 ptw_t2 ptw_t3 ptw_t4 ptw_t5 ptw_t6');
            $(this).addClass('active');
            var theme = $(this).attr( "data-theme" ); 
            ww.addClass('ptw_'+ theme);           
        });
        /* change widget color end */
    }

    /* date picker start */
    if ($('.pt_daterangepicker').length) {
        $('.pt_daterangepicker').daterangepicker({
            opens: "left",
            locale: {
                format: 'MMM D, YYYY'
            },
            startDate: moment().subtract(6, 'days'),
            endDate: moment(),
            ranges: {
                'This Week': [moment().startOf('week').toDate(), moment()],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
    }
    if($('.pt_datetimepicker').length){
        $('.pt_datetimepicker').daterangepicker({
            singleDatePicker: true,
            opens: "right",
            locale: {
                format: 'MMM D, YYYY'
            },
            minDate:moment().add(1, 'days').format('MMM D, YYYY')
        });
    }
    /* date picker end */

    //chat sidebar toggle
    if($('.ptc_sidebar_toggle').length){
        $('.ptc_sidebar_toggle').on('click', function(){
            $('body').addClass('ptc_sidebar_open');
        });
        $(document).mouseup(function(e){
            var container = $(".ptc_sidebar_wrapper");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                $('body').removeClass('ptc_sidebar_open');
            }
        });
        $('.ptc_cu_item').on('click', function(){
            $('body').removeClass('ptc_sidebar_open');
        });
    }

});

$(window).on('load', function(){
    loading(0);
});

function resetMessageScroll(){
    var messageBody = document.querySelector('#messageBody');
    if(messageBody){
        messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
    }
}

function showPopup(t = '', m = '') {
    var a = $('.pt_alert_wrapper');
    $('.pt_alert_text').text(m);
    a.attr('class', 'pt_alert_wrapper alert_open ' + t);
    if (typeof closeAlert != 'undefined') {
        clearTimeout(closeAlert);
    }

    $('.pt_alert_close').one('click', () => {
        a.removeClass('alert_open');
    });

    closeAlert = setTimeout(function() {
        a.removeClass('alert_open');
    }, 3000);
}

function confirm(title = '', text = '', button = ''){
    var el = $('#confirm_popup');
    var el_title = document.querySelector('#confirm_popup .pt_confirm_title');
    var el_text = document.querySelector('#confirm_popup .pt_confirm_text');
    var el_button = document.querySelector('#confirm_popup .pt_confirm_button');
    
    el_title.textContent=title;
    el_text.textContent=text;
    el_button.textContent=button;
    el.modal();
}

function loading(a){
    if(parseInt(a) == 1){
        $('body').addClass('pt_loading');
    }else{
        $('body').removeClass('pt_loading');
    }
}