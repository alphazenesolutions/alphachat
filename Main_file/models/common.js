const con = require('../config/connection');
const commonModel = {}

/*
* Function name - selectData
* Parameters - fieldName,tableName,where
* fieldName - It will be null or commma separated field name
* tableName - Table name
* where - It will be null or SQL conditions.
*/
commonModel.selectData=(fieldName,tableName,where)=>{
    return new Promise((resolve,reject)=>{
        fieldName = (fieldName) ? fieldName : '*';
        var whereParam = '';
        if(where != ''){
            for (const [key, value] of Object.entries(where)) {
                whereParam +=`WHERE ${key} = '${value}'`;
            }
        }
        con.query(`SELECT ${fieldName} FROM ${tableName} ${whereParam} `,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

commonModel.insertData=(tableName,postData)=>{
    return new Promise((resolve,reject)=>{
        con.query(`INSERT INTO ${tableName} SET ?`,postData,(err, result)=>{
            err ? reject(err) : resolve(result);
        });
    });
}

commonModel.updateData=(tableName,postData,where)=>{
    var param = '';
    if(postData != ''){
        for (const [key, value] of Object.entries(postData)) {
            param +=`${key} = '${value}' ,`;
        }
    }

    var whereParam = '';
    for (const [key, value] of Object.entries(where)) {
        whereParam +=`${key} = ${value} `;
    }
    param = param.replace(/,\s*$/, "");
    return new Promise((resolve,reject)=>{
        con.query(`UPDATE ${tableName} SET ${param} WHERE ${whereParam}`,(err, result)=>{
            err ? reject(err) : resolve(result);
        });
    });
}

commonModel.deleteData=(tableName,where)=>{
    var whereParam = '';
        if(where != ''){
            for (const [key, value] of Object.entries(where)) {
                whereParam +=`WHERE ${key} = '${value}'`;
            }
        }
    return new Promise((resolve,reject)=>{
        con.query(`DELETE FROM ${tableName} ${whereParam}`,(err, result)=>{
            err ? reject(err) : resolve(result);
        });
    });
}

module.exports=commonModel

