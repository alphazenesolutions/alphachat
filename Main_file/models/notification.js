const con = require('../config/connection');
const notificationModel = {}

notificationModel.findMsgNotify=(condition)=>{
    var where
    if(condition.project_id){
        where = `WHERE project_id = '${condition.project_id}'`
    }else{
        where = ''
    }
    return new Promise((resolve,reject)=>{
        con.query(`SELECT conversation_id,msg_read,updatedAt FROM pt_notifications ${where}`,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

notificationModel.findData=(fieldName,tableName,condition)=>{
    var param 
    if(condition != ''){
        param = `WHERE project_id IN (${condition.project_id})`
    }else{
        param = ''
    }
    return new Promise((resolve,reject)=>{
        con.query(`SELECT  ${fieldName} FROM ${tableName} ${param} ORDER BY updatedAt DESC LIMIT 25`,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

notificationModel.updateData=(postData,condition)=>{
    return new Promise((resolve,reject)=>{
        let sql = `UPDATE pt_notifications SET msg_read = ? , updatedAt = ? WHERE conversation_id = ? AND type = ?`
        con.query(sql,[`${postData.msg_read}`,`${postData.updatedAt}` ,`${condition.conversation_id}`,`${condition.type}`],(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

notificationModel.selectData=(fieldName,tableName,condition)=>{
    return new Promise((resolve,reject)=>{
        con.query(`SELECT  ${fieldName} FROM ${tableName} WHERE conversation_id = ${condition.conversation_id} AND type = '${condition.type}'`,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

module.exports=notificationModel
