const con = require('../config/connection');

const userModel = {}

userModel.findUser=(condition)=>{
    return new Promise((resolve,reject)=>{
        con.query(`SELECT * FROM pt_users WHERE email='${condition.email}' AND access_level='${condition.access_level}'`,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

userModel.updatePassword=(tableName,postData,condition)=>{
    return new Promise((resolve,reject)=>{
        var sql = `UPDATE ${tableName} set password =? ,resetPasswordToken =? , updatedAt =? WHERE email = ?`
        con.query(sql, [`${postData.password}`,`${postData.resetPasswordToken}`,`${postData.updatedAt}` ,`${condition.email}`], function(err, result) {
            if(err) reject(err)
            resolve(result);
        });
    });
}

userModel.activeOperator=(condition)=>{
    return new Promise((resolve,reject)=>{
        con.query(`SELECT id,name,email,active FROM pt_users WHERE active=${condition.active} AND (NOT id=${condition.id});`, function(err, result) {
            if(err) reject(err)
            resolve(result);
        });
    });
}
module.exports=userModel