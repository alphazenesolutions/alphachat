const con = require('../config/connection');
const customerModel = {}

customerModel.findCustomer=(fieldName,condition)=>{
    return new Promise((resolve,reject)=>{
        con.query(`SELECT ${fieldName} FROM pt_customers WHERE email = '${condition.email}' AND project_id = '${condition.project_id}' `,(err, result)=>{ 
            if(err) reject(err)
            resolve(result);
        });
    });
}

customerModel.getCustomer=(fieldName,condition)=>{
    return new Promise((resolve,reject)=>{
        con.query(`SELECT ${fieldName} FROM pt_customers WHERE id = '${condition.id}' AND project_id = '${condition.project_id}' `,(err, result)=>{ 
            if(err) reject(err)
            resolve(result);
        });
    });
}

customerModel.selectData=(fieldName,tableName,condition)=>{
    return new Promise((resolve,reject)=>{
        con.query(`SELECT ${fieldName} FROM ${tableName} WHERE customer_id = ${condition.customer_id}`,(err, result)=>{ 
            if(err) reject(err)
            resolve(result);
        });
    });
}

module.exports=customerModel