const con = require('../config/connection');
const conversationModel = {}

//find data from conversation table with join
conversationModel.selectDataWithJoin=(condition)=>{
    return new Promise((resolve,reject)=>{
        con.query(`SELECT ptu.name,ptc.message,ptc.operator_id,ptc.createdAt FROM pt_conversations as ptc LEFT JOIN pt_users as ptu
        ON ptc.operator_id = ptu.id WHERE ptc.conversation_id = ${condition.conversation_id}`,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

conversationModel.countCustomerUnreadMsg=(condition)=>{
    return new Promise((resolve,reject)=>{
        con.query(`SELECT COUNT(msg_read) as msg_read FROM pt_conversations WHERE conversation_id = ${condition.conversation_id} AND operator_id=${condition.operator_id} AND msg_read=${condition.msg_read}`,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

conversationModel.countProjectUnreadMsg=(condition)=>{
    return new Promise((resolve,reject)=>{
        con.query(`SELECT COUNT(msg_read) as msg_read FROM pt_conversations WHERE project_id = ${condition.project_id} AND operator_id=${condition.operator_id} AND msg_read=${condition.msg_read}`,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

conversationModel.lastMsgDateTime=(condition)=>{
    return new Promise((resolve,reject)=>{
        con.query(`SELECT createdAt FROM pt_conversations WHERE conversation_id = '${condition.conversation_id}' ORDER BY createdAt DESC LIMIT 1 `,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

conversationModel.selectMsgData=(condition)=>{
    return new Promise((resolve,reject)=>{
        con.query(`SELECT msg_read FROM pt_conversations WHERE conversation_id = ${condition.conversation_id} AND msg_read=${condition.msg_read}`,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

module.exports=conversationModel