const con = require('../config/connection');
const moment = require('moment');
const dashboardModel = {}

dashboardModel.countActiveCustomer=(condition)=>{
    var whereParam 
        if(condition.id){
            whereParam =`WHERE project_id = '${condition.id}' AND active = ${condition.active}`;
        }else{
            whereParam =`WHERE active = ${condition.active}`
        }
    return new Promise((resolve,reject)=>{ 
        con.query(`SELECT COUNT(active) as active FROM pt_customers ${whereParam}`,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

dashboardModel.countCustomer=(where)=>{
    var param
    if(where !=''){
        param = `WHERE project_id = '${where.id}'`
    }else{
        param = ''
    }
    return new Promise((resolve,reject)=>{ 
        con.query(`SELECT COUNT(id) as total_customer FROM pt_customers ${param}`,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

dashboardModel.dateWiseCustomer=(date)=>{
    return new Promise((resolve,reject)=>{ 
        var sql = `SELECT date(createdAt) AS date, createdAt, count(id) AS count FROM pt_customers WHERE createdAt BETWEEN '${moment(date[0]).format('YYYY-MM-DD 00:00:00')}' AND '${moment(date[1]).format('YYYY-MM-DD 23:59:59')}' GROUP BY date`
        con.query(sql,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

dashboardModel.monthWiseCustomer=(date)=>{
    return new Promise((resolve,reject)=>{ 
        var sql = `SELECT month(createdAt) AS month, createdAt, count(id) AS count FROM pt_customers WHERE createdAt BETWEEN '${moment(date[0]).format('YYYY-MM-DD 00:00:00')}' AND '${moment(date[1]).format('YYYY-MM-DD 23:59:59')}' GROUP BY month`
        con.query(sql,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

module.exports=dashboardModel