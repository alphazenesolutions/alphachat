const con = require('../config/connection');
const moment = require('moment');

const projectModel = {}

projectModel.findProject=(fieldName,tableName,where)=>{
    var param 
    if(where != ''){
        param = `WHERE id IN (${where.id})`
    }else{
        param = ''
    }
    return new Promise((resolve,reject)=>{
        con.query(`SELECT ${fieldName} FROM ${tableName} ${param}`,(err, result)=>{
            if(err) reject(err)
            resolve(result);
        });
    });
}

projectModel.updateProjectSetting=(tableName,postData,condition)=>{
    return new Promise((resolve,reject)=>{
        var sql = `UPDATE ${tableName} set project_setting =? , updatedAt =? WHERE project_id = ?`
        con.query(sql, [`${postData.project_setting}`,`${postData.updatedAt}` ,`${condition.project_id}`], function(err, result) {
            if(err) reject(err)
            resolve(result);
        });
    });
}

projectModel.deleteProjectSetting=(where)=>{
    return new Promise((resolve,reject)=>{
        con.query(`DELETE FROM pt_project_settings WHERE project_id=${where.project_id} `,(err, result)=>{
            err ? reject(err) : resolve(result);
        });
    });
}

module.exports=projectModel

